#pragma once

#include <set>
#include "polozka.h"


class Nakupy;

class Sklad
{
    friend Nakupy;
protected:
    unsigned int pocet;
    std::set<Polozka*> polozky;
public:
    Sklad();
    ~Sklad();
    void nacti(std::string soubor);
    unsigned int getPocet() const;
    unsigned int getPocetNezafixovanych() const;
    void vypis() const;
    void vypisNovouCenu() const;
    void vypisRozdilCen() const;
    void vypisNezafixovane() const;
    Polozka* najdiPolozku(unsigned int id);
};
