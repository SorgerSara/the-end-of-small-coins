#include "matrix.h"
#include <vector>
#include <iostream>
#include "Nakupy.h"


Matrix::Matrix(unsigned int radku, unsigned int sloupcu)
    :r(radku), s(sloupcu)
{
    allocateMemory();
    //mat.resize(r, std::vector<int>(s));
    for(unsigned int i=0 ; i<radku ; i++)
    {
        for (unsigned int j=0 ; j<sloupcu ; j++)
        {
            data[i][j] = 0;
        }
    }
}

void Matrix::allocateMemory()
{
    data = new float*[r];
    for(unsigned int i = 0 ; i<r ; i++)
    {
        data[i] = new float[s];
    }
}

void Matrix::tisk() const
{
    for(unsigned int i = 0 ; i<r ; i++)
    {
        for(unsigned int j = 0 ; j<s ; j++)
        {
            std::cout << data[i][j] << "\t";
        }
        std::cout << std::endl;
    }
}


void Matrix::releaseMemory()
{
   if(data)
   {
       for(unsigned int i = 0 ; i<r ; i++)
       {
           delete[] data[i];
       }
       delete[] data;
   }
   data = nullptr;

}


void Matrix::naplnMatici(Nakupy nakupy, Sklad* obchod)
{
    unsigned int i=0;
    for(auto pepuvSeznam : nakupy.nakupy)
    {
        if(!pepuvSeznam->getSafety())
        {
            unsigned int ps = 0;

            for (auto trebaRohlik : pepuvSeznam->seznam)
            {
                ps = ps + trebaRohlik->getCenty();

            }

            unsigned int j=0;
            for(auto vsechnoVObchode : obchod->polozky)
            {
                if(pepuvSeznam->obsahujePolozku(vsechnoVObchode))
                {
                    int policko = vsechnoVObchode->getX();
                    if (policko == 1) ps--; //odecte jednickove konstanty z prave strany
                    else data[i][j] = policko;
                }
                j++;

            }
            data[i++][s-1] = ps;
        }

    }
}

void Matrix::prohodRadky(unsigned int a, unsigned int b)
{
    for(unsigned int i = 0 ; i<s ; i++)
    {
        float tmp = data[a][i];
        data[a][i] = data[b][i];
        data[b][i] = tmp;
    }
}

void Matrix::trojuhelnik()
{
    //jakmile preskocis nejakej radek, tak uz to nebudou diagonalni prvky, tak to predelej, ale zacatek neni spatnej

    for (unsigned int i = 0 ; i<s ; i++)
    {
        if(data[i][i] == 0)
        {
            for(unsigned int j = i ; j<r ; j++)
            {
                if(data[j][i] != 0)
                {
                    //prohodRadky(i,j);
                    j=r;
                }
            }
        }


        /*for(unsigned int k=i+1 ; k<r ; k++)
        {
            //if(data[i][i] == 0) k = r; // v i-tem sloupci jsou pod diagonalou jen nulove prvky
            if(data[i][i]!=0)
            {
                float ki = data[k][i];
                float ii = data[i][i];
                float t = ki/ii; //data[k][i]/data[i][i];
                for (unsigned int j=0 ; j<s ; j++)
                {
                    data[k][j] = data[k][j] - t*data[i][j];
                }
            }
        }
        */


    }
}

