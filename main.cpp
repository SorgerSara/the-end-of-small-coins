#include <iostream>

using namespace std;
#include "polozka.h"
#include "Sklad.h"
#include "Nakupy.h"



int main()
{
    Sklad* lidl = new Sklad();
    lidl->nacti("sortiment2.txt");
    Nakupy* streda = new Nakupy();
    streda->nacti("nakupy6.txt", lidl);

    streda->celyAlgoritmus(lidl);


    delete lidl;
    delete streda;
    return 0;


}
