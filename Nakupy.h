#pragma once

#include<string>
#include<vector>
#include<eigen3/Eigen/Dense>

#include "nakup.h"
#include "Sklad.h"


class Nakupy
{

protected:
    unsigned int pocet, pocetNebezpecnych, t;
    std::vector<Nakup*> nakupy;
    //Sklad* obchod;

    void setT(Sklad* obchod);

    void vypis() const;
    void vypisSPolozkou(Polozka* polozka) const;
    void vypisNebezpecne() const;
    void vypisVysledek(Sklad* obchod) const;

    unsigned int getPocetNebezpecnych() const;

    void naplnMatici(Eigen::MatrixXf* matice, Eigen::VectorXf* vektor, Nakupy* nakupy, Sklad* obchod);

    void fixace(Sklad* obchod);
    void fixujPolozky(Eigen::MatrixXf* matice, Sklad* obchod, Eigen::VectorXf* ps);
    bool hledejPrunik(float p, Eigen::VectorXf* puvodniReseni, Eigen::VectorXf* x, Eigen::VectorXf* noveReseni);
    void zafixujZbytek(Sklad* obchod);
    void aktualizaceCen(Sklad* obchod, Eigen::VectorXf* reseni);
public:
    Nakupy();
    ~Nakupy();

    unsigned int getT() const;

    void nacti(std::string soubor, Sklad* obchod);
    void celyAlgoritmus(Sklad* obchod);





};
