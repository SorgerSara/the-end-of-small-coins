TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        Nakupy.cpp \
        Sklad.cpp \
        main.cpp \
        nakup.cpp \
        polozka.cpp

HEADERS += \
    Nakupy.h \
    Sklad.h \
    nakup.h \
    polozka.h
