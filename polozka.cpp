#include "polozka.h"
#include <string>
#include <cmath>
#include "iostream"

Polozka::Polozka(std::string Nazev, int identif, float cena)
    :nazev(Nazev), id(identif), nakupy(0), pocatecniCena(static_cast<int>(round(cena*100))),
     vyslednaCena(0), centy(pocatecniCena%100), prizeFixed(false)
{}

float Polozka::getCenty() const
{
    return centy;
}

void Polozka::vypis() const
{
    std::cout << id << " " << nazev << " " << this->getPocatecniCena() << std::endl;
}

void Polozka::vypisNovouCenu() const
{
    std::cout << id << " " << nazev << " " << this->getVyslednaCena() << std::endl;
}

void Polozka::vypisRozdilCen() const
{
    std::cout << id << " " << nazev << " " << this->getPocatecniCena() << " -> " << this->getVyslednaCena() << std::endl;
    /*
    if(this->prizeFixed)
    {
        std::cout << id << " " << nazev << " " << this->getPocatecniCena() << " -> " << this->getVyslednaCena() << " T"<< std::endl;
    }
    else std::cout << id << " " << nazev << " " << this->getPocatecniCena() << " -> " << this->getVyslednaCena() << " F"<< std::endl;
    */
}

void Polozka::vypisNazev() const
{
    std::cout << nazev;
}

void Polozka::setCenty(float cena)
{
    if(cena == 0 || cena == 100) ukotviCenty(cena);
    else centy = cena;
}

void Polozka::ukotviCenty(int cena)
{
    centy = cena;
    setVyslednaCena();
    prizeFixed = true;
}

void Polozka::setVyslednaCena()
{
    vyslednaCena = (pocatecniCena - (pocatecniCena%100) + centy);
}

float Polozka::getPocatecniCena() const
{
    return static_cast<float>(pocatecniCena)/100;
}

float Polozka::getVyslednaCena() const
{
    return static_cast<float>(vyslednaCena)/100;
}

float Polozka::getRozdilCen() const
{
    return (getVyslednaCena() - getPocatecniCena());
}
