#pragma once

#include "polozka.h"
#include <set>

class Polozka;
class Nakupy;

class Nakup
{
    friend Nakupy;
protected:
    unsigned int pocetPolozek, nezafixovanePolozky;
    std::set<Polozka*> seznam;
public:
    Nakup();
    void nacti(std::string radek, Sklad* obchod);

    bool getSafety(const Nakupy* nakupy) const;

    void vypis() const;
    void vypisRozdilCen() const;

    bool obsahujePolozku(Polozka* trebaRohlik) const;
    float getRozdilCen() const; // O kolik eur se celkem zemil nakup jednoho zakaznika (po zafixovani vsech cen idealne)
};
