#pragma once

#include <string>
class Sklad;
class Nakup;
class Nakupy;

class Polozka{
    friend Sklad;
    friend Nakup;
    friend Nakupy;
protected:
    std::string nazev;
    unsigned int id, nakupy; //nakupy oznacuje cislo v kolika nakupech se polozka vyskytuje
    int pocatecniCena, vyslednaCena; //s pocatecni cenou moc pracovat nebudeme, jen si ji zachovame pro vysledny vypis
    float centy;
    bool prizeFixed;
public:
    Polozka(std::string nazev, int id, float pocatecniCena);

    void vypis() const;
    void vypisNovouCenu() const;
    void vypisRozdilCen() const;
    void vypisNazev() const;

    void ukotviCenty(int cena);
    void setCenty(float centy);
    void setVyslednaCena();

    float getCenty() const;
    float getPocatecniCena() const;
    float getVyslednaCena() const;
    float getRozdilCen() const;

};

