#pragma once

#include<vector>

class Nakupy;
class Nakup;
class Sklad;

class Matrix
{
    friend Nakupy;
    std::vector<std::vector<float> >mat;
    unsigned int r, s;
    float **data;
public:
    Matrix(unsigned int r, unsigned int s);
    void tisk() const;
    void releaseMemory();
    void allocateMemory();
    void naplnMatici(Nakupy eshop, Sklad* obchod);
    void trojuhelnik();
    void prohodRadky(unsigned int a, unsigned int b);

};


