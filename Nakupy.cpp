#include "Nakupy.h"
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <eigen3/Eigen/Dense>

using namespace Eigen;
using namespace std;

Nakupy::Nakupy()
    : pocet{0}, t{0}
{}

Nakupy::~Nakupy()
{
    for (auto it : nakupy)
    {
        delete it;
    }
}

void Nakupy::nacti(string soubor, Sklad* obchod)
{
    //preda soubor a cislo radku konstruktoru nakup a zvysi pocet o jedna

    ifstream file(soubor);

    if(!file.is_open())
    {
        cout << "soubor se nepodarilo otevrit" << endl;
        return;
    }

    string line;
    while(getline(file, line))
    {
        Nakup* pepuvSeznam = new Nakup();
        pepuvSeznam->nacti(line, obchod);
        nakupy.push_back(pepuvSeznam);
    }
    pocet++;
    file.close();
}

void Nakupy::setT(Sklad* obchod)
{
    for (auto it : obchod->polozky)
    {
        if(it->nakupy > t)
        {
            t = it->nakupy;
        }
    }
}

unsigned int Nakupy::getT() const
{
    return t;
}



//__________________________________________________________________________________________________

void Nakupy::vypis() const
{
    unsigned int i = 1;
    for (auto it : nakupy)
    {
        cout << "nakup no. " << i << ":" << endl;
        it->vypis();
        i++;
        cout << endl;

    }
}

void Nakupy::vypisSPolozkou(Polozka* polozka) const
{
    unsigned int i = 1;
    for(auto it : nakupy)
    {
        it->vypis();
        cout << "polozku ";
        polozka->vypisNazev();

        if(it->obsahujePolozku(polozka))
        {
            cout << " obsahuje" << endl << endl;
        }
        else
        {
            cout << " neobsahuje" << endl << endl;
        }
        i++;
    }

}


void Nakupy::vypisNebezpecne() const
{
    for(auto it : nakupy)
    {
        if(!it->getSafety(this))
        {
            it->vypis();
            cout << endl;

        }
    }
}

//____________________________________________________________________________________________

unsigned int Nakupy::getPocetNebezpecnych() const
{
    unsigned int p=0;
    for (auto it : nakupy)
    {
        if(!it->getSafety(this))
        {
            p++;
        }
    }
    return p;
}

//_______________________________________________________________________________________________


void Nakupy::naplnMatici(MatrixXf* matice, VectorXf* vektor, Nakupy* nakupy, Sklad* obchod)
{
 //radky jsou nebezpecne nakupy, sloupce jeste nezafixovane polozky
    unsigned int i = 0;
    for(auto pepuvSeznam : nakupy->nakupy)
    {
        if(pepuvSeznam->getSafety(this))
        {
            continue;
        }
        unsigned int ps = 0;
        for (auto trebaRohlik : pepuvSeznam->seznam)
        {
            ps = ps + trebaRohlik->getCenty();
        }

        unsigned int j = 0;
        for(auto vsechnoVObchode : obchod->polozky)
        {
            if(vsechnoVObchode->prizeFixed)
            {
                //cena je zafixovana a do matice ji neukladam, ale musim ji odecist od PS
                if((vsechnoVObchode->getCenty() == 100) && (pepuvSeznam->obsahujePolozku(vsechnoVObchode)))
                {
                    ps = ps-100;
                }
            }
            else
            {
                if(pepuvSeznam->obsahujePolozku(vsechnoVObchode))
                {
                    (*matice)(i,j) = 1;
                }
                else
                {
                    (*matice)(i,j) = 0;
                }
                j++;
            }
        }
        (*vektor)(i) = ps;
        (*vektor)(i) = (*vektor)(i)/100;
         i++;
    }
}



//________________________________________________________________________________________



bool Nakupy::hledejPrunik(float p, VectorXf* puvodniReseni, VectorXf* x, VectorXf* noveReseni)
{

    for(unsigned int j = 0 ; j < x->size() ; j++)
    {
        float nove = (*puvodniReseni)(j) + p*((*x)(j));

        if(nove < 0 || nove > 100)
        {
            return false;
        }

        (*noveReseni)(j) = nove;

    }
    return true;
}

void Nakupy::aktualizaceCen(Sklad* obchod, VectorXf* reseni)
{
    unsigned int zMatice = 0;
    for(auto it : obchod->polozky)
    {
        if(!it->prizeFixed)
        {
            it->setCenty((*reseni)(zMatice));
            zMatice++;
        }

    }
}

void Nakupy::fixujPolozky(MatrixXf *matice, Sklad *obchod, VectorXf* ps)
{
    VectorXf* x = new VectorXf(matice->cols());
    (*x) = (matice->colPivHouseholderQr().solve(*ps));
    VectorXf* puvodniReseni = new VectorXf(matice->cols());
    VectorXf* noveReseni = new VectorXf(matice->cols());
    unsigned int i = 0;


    for(auto it : obchod->polozky)
    {
        if(!it->prizeFixed)
        {
            (*puvodniReseni)(i) = it->getCenty();
            i++;

        }
    }

    (*x) = (*x)*100 - (*puvodniReseni);

    for(unsigned int i = 0 ; i<x->size() ; i++)
    {

            float p = (-(*puvodniReseni)(i))/((*x)(i));
            if(hledejPrunik(p, puvodniReseni, x, noveReseni))
            {
                i=x->size();
            }
            else
            {
                p = (100-(*puvodniReseni)(i))/((*x)(i));
                if(hledejPrunik(p, puvodniReseni, x, noveReseni))
                {
                   i = x->size();
                }
            }

    }

   aktualizaceCen(obchod, noveReseni);

   delete x;
   delete puvodniReseni;
   delete noveReseni;

}


void Nakupy::fixace(Sklad* obchod)
{
    MatrixXf* matice = new MatrixXf(getPocetNebezpecnych(), obchod->getPocetNezafixovanych());
    VectorXf* vektor = new VectorXf(getPocetNebezpecnych());
    naplnMatici(matice, vektor, this, obchod);

    fixujPolozky(matice, obchod, vektor);
    delete matice;
    delete vektor;

}

void Nakupy::zafixujZbytek(Sklad* obchod)
{
    for (auto trebaRohlik : obchod->polozky)
    {
        if(!trebaRohlik->prizeFixed)
        {
            if(trebaRohlik->getCenty() >= 50)
            {
                trebaRohlik->ukotviCenty(100);
            }
            else trebaRohlik->ukotviCenty(0);
        }
    }

}

void Nakupy::celyAlgoritmus(Sklad* obchod)
{
    setT(obchod);
    cout << "T = " << getT() << endl;
    cout << "_____________________________________________________" << endl << endl;
    while ((getPocetNebezpecnych()>0)) // zaroven i meni bezpecnost jednotlivych nakupu
    {
        fixace(obchod);
    }
    zafixujZbytek(obchod);
    vypisVysledek(obchod);

}

void Nakupy::vypisVysledek(Sklad* obchod) const
{

    obchod->vypisRozdilCen();
    cout << endl << endl;

    cout << "_____________________________________________________" << endl << endl;

    unsigned int i = 0, idMaxRozdilu;
    float maxRozdil = 0;
    bool vysloTo = true;

    for (auto it : nakupy)
    {
        cout << "nakup no." << ++i << ":" << endl;
        it->vypisRozdilCen();

        float rozdil = it->getRozdilCen();
        if (abs(rozdil) > maxRozdil)
        {
            maxRozdil = abs(rozdil);
            idMaxRozdilu = i;
        }

        cout << "Cena po vymizeni centu z obehu se od te puvodni lisi o " << fixed << setprecision(2) << rozdil << " eur" << endl << endl;

        if(abs(it->getRozdilCen()) > getT()) vysloTo = false;
    }
    cout << "_____________________________________________________" << endl << endl;
    if (vysloTo)
    {
        cout << "Vsechny nakupy se zmenily o maximalne " << getT() << " eur, jako nam tvrdi veta." << endl;
    }
    else
    {
        cout << "Bohuzel se neco pokazilo a nejaky nakup se zmenil o vice nez " << getT() << " eur." << endl;
    }
    cout << "Nejvice se zmenil nakup cislo " << idMaxRozdilu << " a to o ";
    printf("%.2f eur", maxRozdil);
    cout << endl << endl << endl << endl;
}


