#include "nakup.h"
#include "polozka.h"
#include "Sklad.h"
#include "Nakupy.h"

#include <iostream>
#include <fstream>
#include <sstream>

class Nakupy;

Nakup::Nakup()
    :pocetPolozek(0)
{}

void Nakup::nacti(std::string radek, Sklad* obchod)
{
    int id;
    std::istringstream ss(radek);
    while(ss >> id)
    {
        Polozka* a = obchod->najdiPolozku(id);
        seznam.insert(a);
        pocetPolozek++;
    }
    nezafixovanePolozky = pocetPolozek;
}


bool Nakup::getSafety(const Nakupy* nakupy) const
{
    //nebezpecna <-> pokud obsahuje více jak t nezafixovanych promenych
    unsigned int nezafix = 0;
    for(auto it : seznam)
    {
        if(!it->prizeFixed) nezafix++;
    }

    if (nezafix > nakupy->getT() ) return false;

    else return true;
}

void Nakup::vypis() const
{
    for(auto it : seznam)
    {
        it->vypis();
    }
}

void Nakup::vypisRozdilCen() const
{
    for(auto it: seznam)
    {
        it->vypisRozdilCen();
    }
}


bool Nakup::obsahujePolozku(Polozka* trebaRohlik) const
{
    for(auto it : seznam)
    {
        if(it == trebaRohlik) return true;
    }
    return false;

}

//O kolik eur se celkem zemil nakup jednoho zakaznika (po zafixovani vsech cen idealne)
float Nakup::getRozdilCen() const
{    
    float rozdil = 0;
    for (auto it : seznam)
    {
        rozdil = rozdil + it->getRozdilCen();
    }
    return rozdil;
}
