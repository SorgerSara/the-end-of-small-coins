#include"Sklad.h"
#include <iostream>
#include<fstream>
#include<string>
#include"polozka.h"

using namespace std;

Sklad::Sklad()
    :pocet(0)
{}

Sklad::~Sklad()
{
    for(auto it : polozky)
    {
        delete it;
    }

}

void Sklad::nacti(string soubor)
{
    ifstream file(soubor);


    if(file.is_open())
    {
        unsigned int id;
        float cena;
        string nazev;
        Polozka* a;

        while(file >> id >> nazev >> cena)
        {
            //cout << id << " " << nazev << " " << cena << endl;
            a = new Polozka(nazev, id, cena);
            polozky.insert(a);
            pocet++;

        }
        file.close();
    }
    else cout<<"soubor se nepodarilo otevrit" << endl;
}

unsigned int Sklad::getPocetNezafixovanych() const
{
    unsigned int pocet = 0;
    for(auto it : polozky)
    {
        if(!it->prizeFixed)
        {
            pocet++;
        }
    }
    return pocet;
}

Polozka* Sklad::najdiPolozku(unsigned int ide)
{
    for(auto it : polozky)
    {
        if(it->id == ide)
        {
            it->nakupy ++;
            return it;
        }
    }
    return nullptr;

}

unsigned int Sklad::getPocet() const
{
    return pocet;
}


void Sklad::vypis() const
{
    for(auto it : polozky)
    {
        it->vypis();
    }
}

void Sklad::vypisNovouCenu() const
{
    for (auto it : polozky)
    {
        it->vypisNovouCenu();
    }
}

void Sklad::vypisRozdilCen() const
{
    for(auto it : polozky)
    {
        it->vypisRozdilCen();
    }
}

void Sklad::vypisNezafixovane() const
{
    //unsigned int i = 0;
    for(auto it : polozky)
    {
        if(!it->prizeFixed) it->vypis();
        //else std::cout << i++ << std::endl;

    }
}



